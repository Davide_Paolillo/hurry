?Spooky NES Horror Game Asset Pack #2 - UI Supplement
2019 Programancer
Twitter: @Programancer
------------------------------------

Thank you for purchasing the Spooky NES Style Horror Game Asset Pack #2! This pack contains supplemental UI items for use with Asset Pack #1! What does supplemental mean?! It means it's meant to complement the previous pack, and while each of these packs are meant to help get you what you need to *start* a project, this one is more geared toward complementing the existing assets.

For Pack #1: https://programancer.itch.io/spooky-nes-style-horror-asset-pack-1

$1 Non-Commercial License
$*10* Commercial License (less than Pack #1, because this is a supplemental pack!)?

?This pack contains the following elements:

-Spooky Font (with lowercase and uppercase!)
-7 Menu Frames
-Pointers/Cursors
-62 16x16 Menu Icons
-Other Misc Menu Elements
-Example of Use

------------------------------------
This set is designed to help get you started on making your own game and is intended to be expanded by the end-user.?

