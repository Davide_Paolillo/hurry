﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeneficalRandomBuff : MonoBehaviour
{
    [SerializeField] private List<GameObject> buffList;

    private void Awake()
    {
        foreach(GameObject buff in buffList)
        {
            if(buff.GetComponent<BuffFactory>() == null)
            {
                throw new ArgumentException("The game object named : \"" + buff.name + "\" is not a buff, please change or remove the element.");
            }
        }
    }

    private void Start()
    {
        var buffToAdd = buffList[UnityEngine.Random.Range(0, buffList.Count)];
        this.gameObject.AddComponent(buffToAdd.GetComponent<BuffFactory>().GetBuffType());
        Destroy(this);
    }
}
