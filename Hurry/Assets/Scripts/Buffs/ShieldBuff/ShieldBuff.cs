﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBuff : BuffFactory
{
    private Buff shieldBuff;

    private void Start()
    {
        InitializeBuff();
    }

    protected override void InitializeBuff()
    {
        shieldBuff = new Buff(typeOfBuff, buffDurationTime, buffPower);
        userInterfaceObjectToDisplay = PlayerUI.Instance.BubbleUIBuff;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            userInterfaceObjectToDisplay.SetActive(true);
            shieldBuff.BuffStat(collision.gameObject);
            collision.gameObject.GetComponent<PlayerBuffs>().AddPlayerBuff(shieldBuff, userInterfaceObjectToDisplay);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            shieldBuff.BuffStat(collision.gameObject);
            collision.gameObject.GetComponent<EnemyBuffs>().AddEnemyBuff(shieldBuff);
            Destroy(this.gameObject);
        }
    }

    public override Type GetBuffType()
    {
        return typeof(ShieldBuff);
    }
}
