﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSkullBuff : BuffFactory
{
    private Buff fireSkullBuff;

    private void Start()
    {
        InitializeBuff();
    }

    protected override void InitializeBuff()
    {
        fireSkullBuff = new Buff(typeOfBuff, buffDurationTime, buffPower);
        userInterfaceObjectToDisplay = PlayerUI.Instance.SkullUIBuff;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            userInterfaceObjectToDisplay.SetActive(true);
            fireSkullBuff.BuffStat(collision.gameObject);
            collision.gameObject.GetComponent<PlayerBuffs>().AddPlayerBuff(fireSkullBuff, userInterfaceObjectToDisplay);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            fireSkullBuff.BuffStat(collision.gameObject);
            collision.gameObject.GetComponent<EnemyBuffs>().AddEnemyBuff(fireSkullBuff);
            Destroy(this.gameObject);
        }
    }

    public override Type GetBuffType()
    {
        return typeof(FireSkullBuff);
    }
}
