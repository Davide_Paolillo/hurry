﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBuff : BuffFactory
{
    private Buff healthBuff;

    private void Start()
    {
        InitializeBuff();
    }

    protected override void InitializeBuff()
    {
        healthBuff = new Buff(typeOfBuff, buffDurationTime, buffPower);
        userInterfaceObjectToDisplay = PlayerUI.Instance.HealthUIBuff;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            userInterfaceObjectToDisplay.SetActive(true);
            healthBuff.BuffStat(collision.gameObject);
            collision.gameObject.GetComponent<PlayerBuffs>().AddPlayerBuff(healthBuff, userInterfaceObjectToDisplay);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            healthBuff.BuffStat(collision.gameObject);
            collision.gameObject.GetComponent<EnemyBuffs>().AddEnemyBuff(healthBuff);
            Destroy(this.gameObject);
        }
    }

    public override Type GetBuffType()
    {
        return typeof(HealthBuff);
    }
}
