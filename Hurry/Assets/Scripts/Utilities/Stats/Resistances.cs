﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resistances
{
    private float armor;
    private float magicResist;
    private float healthMultiplier;


    public Resistances(float armor, float magicResist, float healthMultiplier)
    {
        this.armor = armor;
        this.magicResist = magicResist;
        this.healthMultiplier = healthMultiplier + 1.0f;
    }

    public Resistances()
    {
        this.armor = 0.0f;
        this.magicResist = magicResist = 0.0f;
        this.healthMultiplier = 1.0f;
    }

    public float Armor { get => armor; }
    public float MagicResist { get => magicResist; }
    public float HealthMultiplier { get => healthMultiplier; }
}
