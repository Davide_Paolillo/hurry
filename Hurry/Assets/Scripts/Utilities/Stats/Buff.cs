﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff
{
    private TypeOfBuff typeOfBuff;
    private BuffDurationTime buffDurationTime;
    private BuffPower buffPower;

    private float attackBuff;
    private float healthBuff;
    private int shieldMaxHits;
    private float buffDuration;

    public TypeOfBuff TypeOfBuff { get => typeOfBuff; }

    public Buff(TypeOfBuff typeOfBuff, BuffDurationTime buffDurationTime, BuffPower buffPower)
    {
        this.typeOfBuff = typeOfBuff;
        this.buffDurationTime = buffDurationTime;
        this.buffPower = buffPower;
        CalculateBuffPower();
    }

    public Buff()
    {
        this.typeOfBuff = TypeOfBuff.HealthBuff;
        this.buffDurationTime = BuffDurationTime.Short;
        this.buffPower = BuffPower.Low;
        CalculateBuffPower();
    }

    private void CalculateBuffPower()
    {
        switch (buffPower)
        {
            case BuffPower.Max:
                attackBuff = 2.0f;
                shieldMaxHits = 10;
                healthBuff = 2.0f;
                break;
            case BuffPower.Medium:
                attackBuff = 1.5f;
                shieldMaxHits = 5;
                healthBuff = 1.5f;
                break;
            case BuffPower.Low:
                attackBuff = 1.25f;
                shieldMaxHits = 3;
                healthBuff = 1.25f;
                break;
            default:
                throw new ArgumentNullException();
        }
    }

    public void BuffStat(GameObject objToBuff)
    {
        switch (typeOfBuff)
        {
            case TypeOfBuff.AttackBuff:
                if (objToBuff.GetComponent<Enemy>())
                {
                    objToBuff.GetComponent<Enemy>().AttackMultiplier = objToBuff.GetComponent<Enemy>().AttackMultiplier * attackBuff;
                }
                else if (objToBuff.GetComponent<Player>())
                {
                    objToBuff.GetComponent<Player>().AttackMultiplier = objToBuff.GetComponent<Player>().AttackMultiplier * attackBuff 
                        >= objToBuff.GetComponent<Player>().MaxAttackMultiplier ? 
                        objToBuff.GetComponent<Player>().MaxAttackMultiplier : objToBuff.GetComponent<Player>().AttackMultiplier * attackBuff;
                }
                break;
            case TypeOfBuff.HealthBuff:
                if (objToBuff.GetComponent<Enemy>())
                {
                    objToBuff.GetComponent<Enemy>().HealthMultiplier = objToBuff.GetComponent<Enemy>().HealthMultiplier * healthBuff;
                }
                else if (objToBuff.GetComponent<Player>())
                {
                    objToBuff.GetComponent<Player>().HealthMultiplier = objToBuff.GetComponent<Player>().HealthMultiplier * healthBuff
                        >= objToBuff.GetComponent<Player>().MaxHealthMultiplier ?
                        objToBuff.GetComponent<Player>().MaxHealthMultiplier : objToBuff.GetComponent<Player>().HealthMultiplier * healthBuff;
                }
                break;
            case TypeOfBuff.ShieldBuff:
                if (objToBuff.GetComponent<Enemy>())
                {
                    objToBuff.GetComponent<Enemy>().BuffShieldHits += shieldMaxHits;
                }
                else if (objToBuff.GetComponent<Player>())
                {
                    objToBuff.GetComponent<Player>().BuffShieldHits += shieldMaxHits;
                }
                break;
            default:
                break;
        }
    }

    public void EndBuff(GameObject objToDebuff)
    {
        switch (typeOfBuff)
        {
            case TypeOfBuff.AttackBuff:
                if (objToDebuff.GetComponent<Enemy>())
                {
                    objToDebuff.GetComponent<Enemy>().AttackMultiplier = objToDebuff.GetComponent<Enemy>().AttackMultiplier / attackBuff;
                }
                else if (objToDebuff.GetComponent<Player>())
                {
                    objToDebuff.GetComponent<Player>().AttackMultiplier = objToDebuff.GetComponent<Player>().AttackMultiplier / attackBuff;
                }
                break;
            case TypeOfBuff.HealthBuff:
                if (objToDebuff.GetComponent<Enemy>())
                {
                    objToDebuff.GetComponent<Enemy>().HealthMultiplier = objToDebuff.GetComponent<Enemy>().HealthMultiplier / healthBuff;
                }
                else if (objToDebuff.GetComponent<Player>())
                {
                    objToDebuff.GetComponent<Player>().AttackMultiplier = objToDebuff.GetComponent<Player>().HealthMultiplier / healthBuff;
                }
                break;
            case TypeOfBuff.ShieldBuff:
                if (objToDebuff.GetComponent<Enemy>() && objToDebuff.GetComponent<Enemy>().BuffShieldHits > 0)
                {
                    objToDebuff.GetComponent<Enemy>().BuffShieldHits = objToDebuff.GetComponent<Enemy>().BuffShieldHits - shieldMaxHits >= 0 ?
                        objToDebuff.GetComponent<Enemy>().BuffShieldHits - shieldMaxHits : 0;
                }
                else if (objToDebuff.GetComponent<Player>() && objToDebuff.GetComponent<Player>().BuffShieldHits > 0)
                {
                    objToDebuff.GetComponent<Player>().BuffShieldHits = objToDebuff.GetComponent<Player>().BuffShieldHits - shieldMaxHits >= 0 ?
                        objToDebuff.GetComponent<Player>().BuffShieldHits - shieldMaxHits : 0;
                }
                break;
            default:
                break;
        }
    }

    public float BuffDuration()
    {
        switch (buffDurationTime)
        {
            case BuffDurationTime.Long:
                buffDuration = 60.0f;
                return buffDuration;
            case BuffDurationTime.Medium:
                buffDuration = 30.0f;
                return buffDuration;
            case BuffDurationTime.Short:
                buffDuration = 10.0f;
                return buffDuration;
            default:
                throw new ArgumentNullException();
        }
    }
}
