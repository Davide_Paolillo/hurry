﻿using UnityEngine;

public static class Utils
{
    public static void DebugRayCast(PolygonCollider2D collider, float extraHeight, Vector3 xPadding, RaycastHit2D hit)
    {
        Color rayColor;
        if (hit.collider != null)
        {
            rayColor = Color.green;
        }
        else
        {
            rayColor = Color.red;
        }
        Debug.DrawRay(collider.bounds.center + new Vector3((collider.bounds.extents - xPadding).x, 0), Vector2.down * (collider.bounds.extents.y + extraHeight), rayColor);
        Debug.DrawRay(collider.bounds.center - new Vector3((collider.bounds.extents - xPadding).x, 0), Vector2.down * (collider.bounds.extents.y + extraHeight), rayColor);
        Debug.DrawRay(collider.bounds.center - new Vector3((collider.bounds.extents - xPadding).x, collider.bounds.extents.y + extraHeight), Vector2.right * (collider.bounds.extents - xPadding).x, rayColor);
    }
}
