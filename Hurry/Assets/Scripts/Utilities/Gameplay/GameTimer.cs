﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : Singleton<GameTimer>
{
    private float gameSessionTimer;
    private float initialGameSessionTime;
    private bool canStartTimer;
    private bool canSaveTime;
    private bool isTimerInPause;

    public float GameSessionTimer { get => gameSessionTimer; }

    private void Start()
    {
        if (FindObjectsOfType<GameTimer>().Length > 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }

        if (PlayerPrefs.HasKey(GlobalVar.LAST_LEVEL_TIMER_PLAYER_PREF))
        {
            PlayerPrefs.DeleteKey(GlobalVar.LAST_LEVEL_TIMER_PLAYER_PREF);
        }

        gameSessionTimer = 0;
        initialGameSessionTime = 0;

        canSaveTime = false;

        StartGameSessionTimer();
    }

    private void Update()
    {
        if (canStartTimer)
        {
            gameSessionTimer = Time.time - initialGameSessionTime;
            //Debug.Log(gameSessionTimer);
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        if (SceneTransitionManager.Instance.GetCurrentSceneName().Equals(SceneTransitionManager.Instance.GetMainMenuScene()))
        {
            StopGameSessionTimer();
            SaveLastLevelTime();
            Destroy(this.gameObject);
        }
    }

    #region TimerOperations
    private void StartGameSessionTimer()
    {
        initialGameSessionTime = Time.time;
        gameSessionTimer = 0;
        canStartTimer = true;
    }

    private void StopGameSessionTimer()
    {
        canStartTimer = false;
    }


    private void SaveLastLevelTime()
    {
        if (canSaveTime)
        {
            PlayerPrefs.SetFloat(GlobalVar.LAST_LEVEL_TIMER_PLAYER_PREF, gameSessionTimer);
            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.DeleteKey(GlobalVar.LAST_LEVEL_TIMER_PLAYER_PREF);
            PlayerPrefs.Save();
        }

        canSaveTime = false;
    }

    public void SaveGameSessionTime()
    {
        canSaveTime = true;
    }
    #endregion
}
