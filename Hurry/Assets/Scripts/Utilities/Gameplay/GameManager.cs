﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [Header("Transition Objects")]
    [SerializeField] private GameObject loseCanvas;
    [SerializeField] private GameObject winCanvas;

    private bool isAlive;

    private Player player;

    public bool IsPlayerAlive { get => isAlive; set => isAlive = value; }
    public GameObject WinCanvas { get => winCanvas; }
    public Player GetPlayer { get => player; }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        player = FindObjectOfType<Player>();
        isAlive = true;

        if (FindObjectsOfType<GameManager>().Length > 1)
        {
            Destroy(this.gameObject);
        }

        DifficultyHandler.Instance.SetCurrentLelvelDifficulty(player, FindObjectsOfType<Enemy>());
    }

    private void Update()
    {
        if (!IfSceneIsMainMenu())
        {
            CheckIfPlayerIsAlive();
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        player = FindObjectOfType<Player>();
        BuffSpawner.Instance.InsantiateRandomBuffsAtRandomPos(level);
        DifficultyHandler.Instance.SetCurrentLelvelDifficulty(FindObjectOfType<Player>(), FindObjectsOfType<Enemy>());
    }

    #region UtilMethods
    private bool IfSceneIsMainMenu()
    {
        if (SceneTransitionManager.Instance.GetCurrentSceneName().Equals(GlobalVar.MAIN_MENU_SCENE_NAME))
        {
            Destroy(this.gameObject);
            return true;
        }
        return false;
    }

    private void CheckIfPlayerIsAlive()
    {
        if (!isAlive && FindObjectsOfType<LoseCanvas>().Length == 0)
        {
            FindObjectOfType<Player>().KillPlayer();
            Instantiate(loseCanvas);
        }
    }
    #endregion
}
