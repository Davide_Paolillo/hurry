﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Fireball)), CanEditMultipleObjects]
public class CustomInspectorFireball : Editor
{
    float spacing;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Fireball playerCon = (Fireball) target;
        GUILayout.Space(spacing);
        EditorGUILayout.LabelField("Fireball Movement", EditorStyles.boldLabel);
        playerCon.shouldFollowPlayer = GUILayout.Toggle(playerCon.shouldFollowPlayer, "Should Follow Player");
        if (playerCon.shouldFollowPlayer)
        {
            GUILayout.Space(spacing);
            playerCon.playerToFollow = EditorGUILayout.ObjectField("Player To Follow", playerCon.playerToFollow, typeof(GameObject), true) as GameObject;
        }
    }
}

[CustomEditor(typeof(Magicball)), CanEditMultipleObjects]
public class CustomInspectorMagicball : Editor
{
    float spacing;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Magicball playerCon = (Magicball)target;
        GUILayout.Space(spacing);
        EditorGUILayout.LabelField("Fireball Movement", EditorStyles.boldLabel);
        playerCon.shouldFollowPlayer = GUILayout.Toggle(playerCon.shouldFollowPlayer, "Should Follow Player");
        if (playerCon.shouldFollowPlayer)
        {
            GUILayout.Space(spacing);
            playerCon.playerToFollow = EditorGUILayout.ObjectField("Player To Follow", playerCon.playerToFollow, typeof(GameObject), true) as GameObject;
        }
    }
}
