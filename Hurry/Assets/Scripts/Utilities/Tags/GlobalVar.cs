﻿using Tags;

public static class GlobalVar
{
    public static string HORIZONTAL_AXIS_NAME { get => PlayerMovement.HORIZONTAL; }
    public static string JUMP_AXIS_NAME { get => PlayerMovement.JUMP; }
    public static string GROUND_TAG { get => FieldTags.GROUND; }
    public static string ROOF_TAG { get => FieldTags.ROOF; }
    public static string IS_WALKING_ANIMATION { get => PlayerAnimations.IS_WALKING; }
    public static string IS_TOUCHING_GROUND_ANIMATION { get => PlayerAnimations.IS_TOUCHING_GROUND; }
    public static string IS_FALLING_ANIMATION { get => PlayerAnimations.IS_FALLING; }
    public static string JUMP_ANIMATION { get => PlayerAnimations.JUMP; }
    public static string GROUND_LAYER_TAG { get => LayerMaskTags.GROUND; }
    public static string PLAYER_TAG { get => FieldTags.PLAYER; }
    public static string PLAYER_KICK { get => PlayerAnimations.KICK; }
    public static string PLAYER_PUNCH { get => PlayerAnimations.PUNCH; }
    public static string ENEMY_TAG { get => EenemyTags.ENEMY; }
    public static string ENEMY_DEATH_TAG { get => EenemyTags.ENEMY_DEATH; }
    public static string PLAYER_BULLET_POOL_TAG { get => PlayerUtilsTags.PLAYER_BULLET_POOL; }
    public static string PLAYER_BULLET_TAG { get => BulletTags.PLAYER_BULLET; }
    public static string ENEMY_BULLET_TAG { get => BulletTags.ENEMY_BULLET; }
    public static string PLAYER_KICK_ANIMATION { get => PlayerAnimations.PLAYER_KICK_ANIMATION; }
    public static string PLAYER_PUNCH_ANIMATION { get => PlayerAnimations.PLAYER_PUNCH_ANIMATION; }
    public static string IS_PLAYER_HIT_ANIMATION { get => PlayerAnimations.IS_PLAYER_HIT; }
    public static string IS_DEAD_ANIMATION { get => PlayerAnimations.IS_DEAD; }
    public static string MAIN_MENU_SCENE_NAME { get => SceneTags.MAIN_MENU_SCENE; }
    public static string OPEN_OPTIONS_MENU_ANIMATION { get => UIAnimations.OPEN_OPTIONS_MENU; }
    public static string CLOSE_OPTIONS_MENU_ANIMATION { get => UIAnimations.CLOSE_OPTIONS_MENU; }
    public static string OPEN_HAMBURGER_MENU_ANIMATION { get => UIAnimations.OPEN_HAMBURGER_MENU; }
    public static string CLOSE_HAMBURGER_MENU_ANIMATION { get => UIAnimations.CLOSE_HAMBURGER_MENU; }
    public static string LEVEL_ONE_SCENE { get => SceneTags.LEVEL_ONE_SCENE; }
    public static string PLAYER_CROUCH_ANIMATION { get => PlayerAnimations.IS_CROUCHED; }
    public static string PLAYER_CROUCH_KICK_ANIMATION { get => PlayerAnimations.PLAYER_CROUCH_KICK; }
    public static string BUFF_SPAWN_AREA_TAG { get => BuffTags.BUFF_SPAWN_AREA_TAG; }
    public static string EASY_DIFFICULTY_LABEL { get => DifficultyLabels.EASY_MODE; }
    public static string NORMAL_DIFFICULTY_LABEL { get => DifficultyLabels.NORMAL_MODE; }
    public static string HARD_DIFFICULTY_LABEL { get => DifficultyLabels.HARD_MODE; }
    public static string PLAYER_PREF_DIFFICULTY_NAME { get => DifficultyTags.PLAYER_PREF_DIFFICULTY; }
    public static string ENEMY_BULLET_POOL_TAG { get => EnemyUtilsTags.ENEMY_BULLET_POOL; }
    public static string LAST_LEVEL_TIMER_PLAYER_PREF { get => PlayerScores.LAST_LEVEL_TIMER; }
    public static string LAST_LEVEL_SCORE_PLAYER_PREF { get => PlayerScores.LAST_LEVEL_SCORE; }
    public static string BEST_SCORE_PLAYER_PREF { get => PlayerScores.BEST_SCORE; }
    public static string OPEN_SCORE_TEXT_ANIMATION { get => UIAnimations.OPEN_SCORE_TEXT; }
    public static string CLOSE_SCORE_TEXT_ANIMATION { get => UIAnimations.CLOSE_SCORE_TEXT; }
    public static string BEST_SCORE_TEXT_TAG { get => UITags.BEST_SCORE_TEXT; }
    public static string LAST_SCORE_TEXT_TAG { get => UITags.LAST_SCORE_TEXT; }
}
