﻿namespace Tags
{
    public readonly struct FieldTags
    {
        public readonly static string GROUND = "Ground";
        public readonly static string ROOF = "Roof";
        public readonly static string PLAYER = "Player";
    }

    public readonly struct PlayerMovement
    {
        public readonly static string HORIZONTAL = "Horizontal";
        public readonly static string JUMP = "Jump";
    }

    public readonly struct PlayerAnimations
    {
        public readonly static string IS_WALKING = "IsWalking";
        public readonly static string IS_CROUCHED = "IsCrouched";
        public readonly static string IS_TOUCHING_GROUND = "IsTouchingGround";
        public readonly static string IS_FALLING = "IsFalling";
        public readonly static string JUMP = "Jump";
        public readonly static string KICK = "Kick";
        public readonly static string PUNCH = "Punch";
        public readonly static string IS_PLAYER_HIT = "IsPlayerHit";
        public readonly static string IS_DEAD = "IsDead";
        public readonly static string PLAYER_KICK_ANIMATION = "PlayerKickAttack";
        public readonly static string PLAYER_CROUCH_KICK = "CrouchedKick";
        public readonly static string PLAYER_PUNCH_ANIMATION = "PlayerPunchAttack";
    }

    public readonly struct LayerMaskTags
    {
        public readonly static string GROUND = "Ground";
    }

    public readonly struct EenemyTags
    {
        public readonly static string ENEMY = "Enemy";
        public readonly static string ENEMY_DEATH = "IsDead";
    }

    public readonly struct PlayerUtilsTags
    {
        public readonly static string PLAYER_BULLET_POOL = "PlayerBulletPool";
    }

    public readonly struct EnemyUtilsTags
    {
        public readonly static string ENEMY_BULLET_POOL = "EnemyBulletPool";
    }

    public readonly struct BulletTags
    {
        public readonly static string PLAYER_BULLET = "PlayerBullet";
        public readonly static string ENEMY_BULLET = "EnemyBullet";
    }

    public readonly struct SceneTags
    {
        public readonly static string MAIN_MENU_SCENE = "MainMenu";
        public readonly static string LEVEL_ONE_SCENE = "LevelOne";
    }

    public readonly struct UIAnimations
    {
        public readonly static string OPEN_OPTIONS_MENU = "OpenOptions";
        public readonly static string CLOSE_OPTIONS_MENU = "CloseOptions";
        public readonly static string OPEN_HAMBURGER_MENU = "In";
        public readonly static string CLOSE_HAMBURGER_MENU = "Out";
        public readonly static string OPEN_SCORE_TEXT = "ScoreIn";
        public readonly static string CLOSE_SCORE_TEXT = "ScoreOut";
    }

    public readonly struct UITags
    {
        public readonly static string LAST_SCORE_TEXT = "LastScore";
        public readonly static string BEST_SCORE_TEXT = "BestScore";
    }

    public readonly struct BuffTags
    {
        public readonly static string BUFF_SPAWN_AREA_TAG = "BuffSpawnArea";
    }

    public readonly struct DifficultyLabels
    {
        public readonly static string EASY_MODE = "Easy";
        public readonly static string NORMAL_MODE = "Normal";
        public readonly static string HARD_MODE = "Hard";
    }

    public readonly struct DifficultyTags
    {
        public readonly static string PLAYER_PREF_DIFFICULTY = "Difficulty";
    }

    public readonly struct PlayerScores
    {
        public readonly static string LAST_LEVEL_TIMER = "LastLevelTimer";
        public readonly static string LAST_LEVEL_SCORE = "LastLevelScore";
        public readonly static string BEST_SCORE = "BestScore";
    }
}
