﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionManager : Singleton<SceneTransitionManager>
{
    private void Start()
    {
        if (FindObjectsOfType<SceneTransitionManager>().Length > 1)
        {
            Destroy(this.gameObject);
        }
    }

    public void GoToScene(string sceneName, List<GameObject> objToMove)
    {
        SceneManager.LoadSceneAsync(sceneName); // Carica la scena in background

        SceneManager.sceneLoaded += (newScene, mode) =>
        {
            SceneManager.SetActiveScene(newScene);
        }; // Lambda, quando finisce di caricare la nuova scena in background allora esegue il comando

        Scene sceneToLoadObjectsInside = SceneManager.GetSceneByName(sceneName);
        foreach (GameObject obj in objToMove)
        {
            SceneManager.MoveGameObjectToScene(obj, sceneToLoadObjectsInside);
        }
    }

    public void GoToNextScene(int sceneNumber, List<GameObject> objToMove)
    {
        SceneManager.LoadSceneAsync(sceneNumber); // Carica la scena in background

        SceneManager.sceneLoaded += (newScene, mode) =>
        {
            SceneManager.SetActiveScene(newScene);
        }; // Lambda, quando finisce di caricare la nuova scena in background allora esegue il comando


        Scene sceneToLoadObjectsInside = SceneManager.GetSceneByBuildIndex(sceneNumber);
        foreach (GameObject obj in objToMove)
        {
            SceneManager.MoveGameObjectToScene(obj, sceneToLoadObjectsInside);
        }
    }

    public void ReloadCurrentScene(List<GameObject> gameObjectToImport)
    {
        GoToScene(SceneManager.GetActiveScene().name, gameObjectToImport);
    }

    public int GetNextSceneBuildIndex()
    {
        return SceneManager.GetActiveScene().buildIndex + 1;
    }

    public int GetCurrentSceneBuildIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public string GetCurrentSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public int GetSceneCountInBuild()
    {
        return SceneManager.sceneCountInBuildSettings;
    }

    public string GetMainMenuScene()
    {
        return SceneManager.GetSceneByName(GlobalVar.MAIN_MENU_SCENE_NAME).name;
    }

    public int GetMainMenuSceneBuildIndex()
    {
        return 0; // First scene in build index, not optimal solution, but needed in order to allow the program to work
    }
}
