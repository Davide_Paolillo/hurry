﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BeneficalSources : MonoBehaviour
{
    [Header("Statistics")]
    [Range(0.1f, 10.0f)] [SerializeField] private float regenPower = 5.0f;

    [Header("Timers")]
    [Range(0.1f, 2.0f)] [SerializeField] private float cooldownBetweenRegenPops = 1.0f;
    [Range(1.0f, 30.0f)] [SerializeField] private float timeToLive = 10.0f;

    private bool canPop;
    private bool isRunning;

    protected bool CanPop { get => canPop; set => canPop = value; }

    public float RegenerationPower { get => regenPower; }
    public float CooldownBetweenRegenPops { get => cooldownBetweenRegenPops; }
    public float TimeToLive { get => timeToLive; }

    private void Awake()
    {
        canPop = false;
        isRunning = false;
        CheckConditions();
    }

    #region Initializations
    private void CheckConditions()
    {
        cooldownBetweenRegenPops = cooldownBetweenRegenPops <= 0 ? 0.1f : cooldownBetweenRegenPops > 2.0f ? 2.0f : cooldownBetweenRegenPops;
        regenPower = regenPower <= 0 ? 0.1f : regenPower > 10.0f ? 10.0f : regenPower;
        timeToLive = timeToLive < 1.0f ? 1.0f : timeToLive > 30.0f ? 30.0f : timeToLive;
    }
    #endregion

    private void Update()
    {
        CheckConditions();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            canPop = true;
            if (!isRunning)
            {
                isRunning = true;
                RegenStat();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            canPop = false;
        }
    }

    protected abstract void RegenStat();
    protected abstract IEnumerator CastRegenerationStat();
    protected abstract IEnumerator RegenerationPops();
}
