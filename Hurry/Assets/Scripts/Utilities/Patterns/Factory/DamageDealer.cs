﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageDealer : MonoBehaviour
{
    protected float damage;
    protected TypeOfAttack typeOfBullet;

    private void Start()
    {
        if (GetComponent<Bullet>())
        {
            damage = GetComponent<Bullet>().BulletDamage;
            typeOfBullet = GetComponent<Bullet>().BulletType;
        }
    }

    public abstract void DealDamage(GameObject enemy);

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG) || collision.gameObject.tag.Equals(GlobalVar.ROOF_TAG))
        {
            Destroy(this.gameObject);
        }
    }
}
