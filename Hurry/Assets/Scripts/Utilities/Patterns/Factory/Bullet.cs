﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    [Header("Bullet Stats")]
    [SerializeField] private float bulletDamage = 0.0f;
    [SerializeField] protected float bulletSpeed = 10.0f;
    [SerializeField] protected float attackRange = 1.0f;
    [SerializeField] private TypeOfAttack bulletType;

    [Header("Bullet Owner")]
    [SerializeField] private TypeOfBullet typeOfBullet;

    public float BulletDamage { get => bulletDamage; }
    public TypeOfAttack BulletType { get => bulletType; }
    public float SetBulletDamage { get => bulletDamage; set => bulletDamage = value; }

    private void Awake()
    {
        switch (typeOfBullet)
        {
            case TypeOfBullet.EnemyBullet:
                this.gameObject.AddComponent<EnemyDamageDealer>();
                break;
            case TypeOfBullet.PlayerBullet:
                this.gameObject.AddComponent<PlayerDamageDealer>();
                break;
            default:
                break;
        }
    }

    protected abstract void ShotBullet();
}
