﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack : MonoBehaviour
{
    [Header("Attack Statistics")]
    [SerializeField] protected float damage;
    [SerializeField] protected float raycastDistance = 50.0f;
    [SerializeField] protected LayerMask enemyLayer;
    [SerializeField] protected TypeOfAttack typeOfAttack;

    private float startingDamage;

    protected PlayerAttack playerAttack;

    protected bool canAttack;

    public bool CanAttack { get => canAttack; }

    private void Start()
    {
        startingDamage = damage;
        damage *= GameManager.Instance.GetPlayer.AttackMultiplier;
        playerAttack = FindObjectOfType<PlayerAttack>();
    }

    private void Update()
    {
        if((damage / startingDamage) != GameManager.Instance.GetPlayer.AttackMultiplier)
        {
            damage = startingDamage * GameManager.Instance.GetPlayer.AttackMultiplier;
        }
    }

    protected abstract void DealDamage(Enemy enemy);
    protected abstract bool CheckContact();
}
