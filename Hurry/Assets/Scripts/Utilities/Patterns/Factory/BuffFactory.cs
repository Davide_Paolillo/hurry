﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BuffFactory : MonoBehaviour
{
    [Header("Buff Stats")]
    [SerializeField] protected TypeOfBuff typeOfBuff;
    [SerializeField] protected BuffPower buffPower;
    [SerializeField] protected BuffDurationTime buffDurationTime;

    protected GameObject userInterfaceObjectToDisplay;

    protected abstract void InitializeBuff();
    public abstract Type GetBuffType();
}
