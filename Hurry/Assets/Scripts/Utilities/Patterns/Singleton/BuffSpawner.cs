﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpawner : Singleton<BuffSpawner>
{
    [Header("Spanable Buffs")]
    [SerializeField] private List<GameObject> buffList;

    private List<Vector3> spawnedPos;
    private PolygonCollider2D spawnArea;

    private void Awake()
    {
        if (FindObjectsOfType<BuffSpawner>().Length > 1)
        {
            Destroy(this);
        }
        else
        {
            spawnedPos = new List<Vector3>();
            DontDestroyOnLoad(this);
        }
    }

    public void InsantiateRandomBuffsAtRandomPos(int level)
    {
        var numberOfBuffsToSpawn = level > 3 ? UnityEngine.Random.Range(0, 3) : UnityEngine.Random.Range(0, level);
        for(int i = 0; i < numberOfBuffsToSpawn; i++)
        {
            InstantiateRandomBuff();
        }
    }

    private void InstantiateRandomBuff()
    {
        var randomBuff = buffList[UnityEngine.Random.Range(0, buffList.Count)];
        if (!SceneTransitionManager.Instance.GetCurrentSceneName().Equals(SceneTransitionManager.Instance.GetMainMenuScene()))
        {
            spawnArea = GameObject.FindGameObjectWithTag(GlobalVar.BUFF_SPAWN_AREA_TAG).GetComponent<PolygonCollider2D>();
            Vector3 myBuffPos = CalculateNewPos();
            GameObject myBuff = Instantiate(randomBuff, myBuffPos, Quaternion.identity);
            var hit = Physics2D.Raycast(myBuff.transform.position, -Vector3.up, 10000.0f, LayerMask.GetMask(GlobalVar.GROUND_LAYER_TAG));
            if (hit.collider == null)
            {
                Destroy(myBuff);
            }
        }
    }

    private bool CheckIfPosIsValid(Vector3 myBuffPos)
    {
        if(spawnedPos.ToArray().Length > 0)
        {
            foreach(Vector3 pos in spawnedPos)
            {
                if (pos == myBuffPos)
                {
                    return false;
                }
            }
        }
        spawnedPos.Add(myBuffPos);
        return true;
    }

    private Vector3 CalculateNewPos()
    {
        Vector3 tmp = new Vector3();
        do
        {
            tmp = new Vector3(UnityEngine.Random.Range(spawnArea.bounds.min.x, spawnArea.bounds.max.x), spawnArea.bounds.max.y, spawnArea.bounds.min.z);
        }
        while (!CheckIfPosIsValid(tmp));
        return tmp;
    }
}
