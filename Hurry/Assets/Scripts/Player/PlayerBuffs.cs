﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBuffs : MonoBehaviour
{
    private List<Buff> buffList;

    private void Start()
    {
        buffList = new List<Buff>();
    }

    public void AddPlayerBuff(Buff buff, GameObject userInterfaceObjToDeactivate)
    {
        buffList.Add(buff);
        StartCoroutine(DecayBuff(buff, userInterfaceObjToDeactivate));
    }


    private IEnumerator DecayBuff(Buff buff, GameObject userInterfaceObjToDeactivate)
    {
        if(buff.TypeOfBuff == TypeOfBuff.ShieldBuff)
        {
            StartCoroutine(CheckBuffIntegrity(buff, userInterfaceObjToDeactivate));
        }
        yield return new WaitForSeconds(buffList[buffList.IndexOf(buff)].BuffDuration());
        StopCoroutine(CheckBuffIntegrity(buff, userInterfaceObjToDeactivate));
        buffList[buffList.IndexOf(buff)].EndBuff(this.gameObject);
        buffList.RemoveAt(buffList.IndexOf(buff));
        UIOperations(buff, userInterfaceObjToDeactivate);
    }

    private IEnumerator CheckBuffIntegrity(Buff buff, GameObject userInterfaceObjToDeactivate)
    {
        while (true)
        {
            UIOperations(buff, userInterfaceObjToDeactivate);
            yield return new WaitForSeconds(0.1f);
        }
    }

    #region UIOperations
    private void UIOperations(Buff buff, GameObject userInterfaceObjToDeactivate)
    {
        if (GetComponent<Player>().BuffShieldHits <= 0)
        {
            userInterfaceObjToDeactivate.SetActive(false);
        }
    }
    #endregion
}
