﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Main Stats")]
    [Range(0.0f, 200.0f)][SerializeField] private float health = 100.0f;
    [Range(0.0f, 100.0f)][SerializeField] private float shield = 0.0f;

    [Header("Buffable Stats")]
    [SerializeField] private int buffShieldHits; //TODO cancel serialize field for debug
    [Range(1.0f, 2.0f)][SerializeField] private float healthMultiplier = 1.0f;
    [Range(1.0f, 2.0f)] [SerializeField] private float attackMultiplier = 1.0f;

    [Header("Buff items")]
    [SerializeField] private GameObject buffShield;

    private PlayerAnimationController playerAnimationController;

    private Resistances playerResistance;

    private float startingHealth;
    private float healthPercentage;
    private float maxShield;
    private float startingHealthMultiplier;
    private float maxAttackMultiplier;
    private float maxHealthMultiplier;
    private float exp;
    private float requiredExp;
    private int level;
    private int levelBase;

    private bool isAlive;

    private void Awake()
    {
        maxShield = 100.0f; // Need to do that here due to execution order purposes, i need it initialize before everything starts to run
    }

    private void Start()
    {
        CheckFields();
        InitializePlayerStats();
        InitializePlayerResistances();
        CachedPatameters();
    }


    #region InitializationFunctions
    private void InitializePlayerStats()
    {
        health *= healthMultiplier;
        startingHealthMultiplier = healthMultiplier;
        startingHealth = health;
        buffShieldHits = 0;
        isAlive = true;
        exp = 0.0f;
        levelBase = 100;
        level = (int)(exp / levelBase);
        requiredExp = levelBase * level;
        maxAttackMultiplier = 2.0f;
        maxHealthMultiplier = 2.0f;
        healthPercentage = Mathf.RoundToInt(((health / healthMultiplier) * 100) / startingHealth);
    }

    private void InitializePlayerResistances()
    {
        playerResistance = new Resistances();
    }

    private void CheckFields()
    {
        if (attackMultiplier < 0.5f)
        {
            attackMultiplier = 0.5f;
        }

        if(healthMultiplier < 1.0f)
        {
            healthMultiplier = 1.0f;
        }
    }

    private void CachedPatameters()
    {
        playerAnimationController = GetComponent<PlayerAnimationController>();
    }
    #endregion

    private void Update()
    {
        CheckIfPlayerIsAlive();
        ToggleBuffShield();
        CalculateCurrentHealthPercentage();
    }

    #region BuffOperations
    private void ToggleBuffShield()
    {
        if (buffShieldHits > 0)
        {
            buffShield.SetActive(true);
        }
        else
        {
            buffShieldHits = 0;
            buffShield.SetActive(false);
        }
    }

    private void CalculateCurrentHealthPercentage()
    {
        if (startingHealthMultiplier != healthMultiplier)
        {
            health = (startingHealth * healthMultiplier * healthPercentage) / 100;
            startingHealthMultiplier = healthMultiplier;
        }

        healthPercentage = ((health / healthMultiplier) * 100) / startingHealth;
    }

    #endregion

    #region PlayerRelatedOperations
    private float CalculateNewHealthPercentage(float myHealth)
    {
        if (startingHealthMultiplier != healthMultiplier)
        {
            myHealth = (startingHealth * healthMultiplier * healthPercentage) / 100;
            startingHealthMultiplier = healthMultiplier;
        }

       return ((myHealth / healthMultiplier) * 100) / startingHealth;
    }

    private float CalculateNewShieldPercentage(float myShield)
    {
        return (myShield * 100) / maxShield;
    }

    private float CalculateMaxHealth()
    {
        return startingHealth * healthMultiplier;
    }

    private void CheckIfPlayerIsAlive()
    {
        if (health <= 0.0f)
        {
            isAlive = false;
            playerAnimationController.PlayDeathAnimation();
            GameManager.Instance.IsPlayerAlive = isAlive;
        }
    }

    public void KillPlayer()
    {
        health = 0.0f;
    }
    #endregion

    #region Getters

    public float Exp { get => exp; }
    public int Level { get => level; }
    public int LevleBase { get => levelBase; }
    public float RequiredExp { get => requiredExp; }
    public bool IsAlive { get => isAlive; }
    public float MaxAttackMultiplier { get => maxAttackMultiplier; }
    public float MaxHealthMultiplier { get => maxHealthMultiplier; }
    public int BuffShieldHits { get => buffShieldHits; set => buffShieldHits = value; }
    public float AttackMultiplier { get => attackMultiplier; set => attackMultiplier = value; }
    public float HealthMultiplier { get => healthMultiplier; set => healthMultiplier = value; }
    public float MaxShield { get => maxShield; }

    public float GetCurrentHealthPercentage()
    {
        return healthPercentage;
    }

    public float GetCurrentShieldPercentage()
    {
        return ((shield * 100.0f) / maxShield) > 0 ? ((shield * 100.0f) / maxShield) : 0;
    }
    #endregion

    #region ExternalOperations
    public void GetDamage(float damage, TypeOfAttack typeOfAttack)
    {
        if (buffShieldHits <= 0 && shield <= 0)
        {
            health -= damage;
            playerAnimationController.PlayHitAnimation();
        }
        else if(buffShieldHits > 0)
        {
            buffShieldHits -= 1;
        }
        else
        {
            var remainingShield = shield - damage;
            shield =  remainingShield < 0 ? 0 : shield - damage;
            health = remainingShield < 0 ? health - (remainingShield * -1) : health;
            playerAnimationController.PlayHitAnimation();
        }
    }

    public void HealPlayer(float heal)
    {
        health = CalculateNewHealthPercentage(heal + health) >= 100.0f ? CalculateMaxHealth() : health + heal;
    }

    public void RegeneratePlayerShield(float shieldToAdd)
    {
        shield = CalculateNewShieldPercentage(shield + shieldToAdd) >= 100.0f ? maxShield : shield + shieldToAdd;
    }
    #endregion
}
