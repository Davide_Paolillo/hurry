﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionsHandler : MonoBehaviour
{
    [SerializeField] private LayerMask gorundLayer;
    [SerializeField] private float rayExtraHeight = 0.1f;
    [SerializeField] private float rayXPadding = 0.5f;

    private CapsuleCollider2D playerCollider;

    private Vector3 xPadding;

    private bool isTouchingGround;

    public bool IsTouchingGround { get => isTouchingGround; }

    private void Start()
    {
        playerCollider = GetComponent<CapsuleCollider2D>();
        isTouchingGround = false;
        xPadding = new Vector3(rayXPadding, 0, 0);
    }

    private void FixedUpdate()
    {
        RaycastToCheckIfPlayerIsGrounded();
    }

    private void RaycastToCheckIfPlayerIsGrounded()
    {
        /*extents is exactly half of the height of our collider*/
        var hit = Physics2D.BoxCast(playerCollider.bounds.center, playerCollider.bounds.size - xPadding, 0, Vector2.down, rayExtraHeight, gorundLayer);
        if (hit.collider != null && hit.collider.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            isTouchingGround = true;
        }
        else
        {
            isTouchingGround = false;
        }
    }
}
