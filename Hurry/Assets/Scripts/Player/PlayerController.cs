﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement Controller")]
    [SerializeField] private float movementSpeed = 1.0f;

    [Header("Jump Controller")]
    [SerializeField] private float jumpForce = 250.0f;
    [SerializeField] private float lowJumpGravityMultiplier = 3.0f;
    [SerializeField] private float fallJumpGravityMultiplier = 2.5f;

    private Rigidbody2D player;
    private PlayerCollisionsHandler playerCollisions;

    private Vector2 movement;

    private bool canJump;
    private bool isTouchingGround;
    private bool isStillPressingJump;
    private bool isRunning;

    public float MovementSpeed { get => movementSpeed; set => movementSpeed = value; }
    public float JumpForce { get => jumpForce; set => jumpForce = value; }

    void Start()
    {
        BooleanInitializations();
        CacheVriables();
        ParameterInstantiations();
    }


    #region Initializations
    private void ParameterInstantiations()
    {
        movement = new Vector2();
    }

    private void BooleanInitializations()
    {
        isRunning = false;
        canJump = false;
        isStillPressingJump = false;
    }
    #endregion

    #region ChachedVariables
    private void CacheVriables()
    {
        player = GetComponent<Rigidbody2D>();
        playerCollisions = GetComponent<PlayerCollisionsHandler>();
    }
    #endregion

    void Update()
    {
        GetMovements();
    }

    #region GetInputs
    private void CheckIfCanJump()
    {
        isTouchingGround = playerCollisions.IsTouchingGround;
        canJump = movement.y > 0 && isTouchingGround && !isStillPressingJump ? true : false;
        if (canJump && !isRunning)
        {
            isRunning = true;
            StartCoroutine(CheckJumpPress());
        }
    }

    #region Coroutines
    private IEnumerator CheckJumpPress()
    {
        while (movement.y > 0)
        {
            isStillPressingJump = true;
            yield return null;
        }
        isStillPressingJump = false;
        isRunning = false;
    }
    #endregion

    private void GetMovements()
    {
        movement.x = Input.GetAxisRaw(GlobalVar.HORIZONTAL_AXIS_NAME);
        movement.y = Input.GetAxisRaw(GlobalVar.JUMP_AXIS_NAME);
    }
    #endregion

    private void FixedUpdate()
    {
        CheckIfCanJump(); // Used here cos inside the update it causes weird bugs
        
        Move();

        if (canJump)
        {
            Jump();
        }

        SmoothJumpBasedOnKeyPressure();
    }

    #region PhysicsHandlerFunctions
    private void SmoothJumpBasedOnKeyPressure()
    {
        if (player.velocity.y > 0 && movement.y == 0)
        {
            player.velocity += Vector2.up * Physics2D.gravity * lowJumpGravityMultiplier * Time.fixedDeltaTime;
        }
        else if (player.velocity.y < 0)
        {
            player.velocity += Vector2.up * Physics2D.gravity * fallJumpGravityMultiplier * Time.fixedDeltaTime;
        }
    }

    private void Move()
    {
        player.velocity = new Vector2(movement.x * movementSpeed, this.player.velocity.y * Time.timeScale);
    }

    private void Jump()
    {
        player.velocity = new Vector2(this.player.velocity.x, 0.0f);
        player.AddForce(new Vector2(0.0f, jumpForce));
    }
    #endregion
}
