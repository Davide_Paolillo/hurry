﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    private Animator animator;
    private Rigidbody2D player;
    private PlayerCollisionsHandler playerCollisions;

    private void Start()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<Rigidbody2D>();
        playerCollisions = GetComponent<PlayerCollisionsHandler>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            animator.SetBool(GlobalVar.PLAYER_CROUCH_ANIMATION, true);
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            animator.SetBool(GlobalVar.PLAYER_CROUCH_ANIMATION, false);
            GetComponent<PlayerAttack>().ResetMovementSpeed();
        }

        if (player.velocity.x != 0 && Input.GetAxisRaw(GlobalVar.HORIZONTAL_AXIS_NAME) < 0)
        {
            // Flipping the player on the x axis mantaining his native scale with abs value (ex. if i have a scale of (2, 2, 2) it will persist even if it's flipped)
            var newScale = new Vector3(-1 * Mathf.Abs(this.transform.localScale.x), 1 * Mathf.Abs(this.transform.localScale.y), 1 * Mathf.Abs(this.transform.localScale.y));
            this.transform.localScale = newScale;
            animator.SetBool(GlobalVar.IS_WALKING_ANIMATION, true);
        }
        else if(player.velocity.x != 0 && Input.GetAxisRaw(GlobalVar.HORIZONTAL_AXIS_NAME) > 0)
        {
            // Flipping the player on the x axis mantaining his native scale with abs value (ex. if i have a scale of (2, 2, 2) it will persist even if it's flipped)
            var newScale = new Vector3(1 * Mathf.Abs(this.transform.localScale.x), 1 * Mathf.Abs(this.transform.localScale.y), 1 * Mathf.Abs(this.transform.localScale.y));
            this.transform.localScale = newScale;
            animator.SetBool(GlobalVar.IS_WALKING_ANIMATION, true);
        }
        else if (Input.GetAxisRaw(GlobalVar.HORIZONTAL_AXIS_NAME) == 0)
        {
            animator.SetBool(GlobalVar.IS_WALKING_ANIMATION, false);
        }

        animator.SetBool(GlobalVar.IS_TOUCHING_GROUND_ANIMATION, playerCollisions.IsTouchingGround);

        if (playerCollisions.IsTouchingGround)
        {
            animator.SetBool(GlobalVar.IS_FALLING_ANIMATION, false);
        }
    }

    private void FixedUpdate()
    {
        if (player.velocity.y > 1.0f)
        {
            animator.SetTrigger(GlobalVar.JUMP_ANIMATION);
        }

        if (player.velocity.y < 0.0f)
        {
            animator.ResetTrigger(GlobalVar.JUMP_ANIMATION);
            animator.SetBool(GlobalVar.IS_FALLING_ANIMATION, true);
        }
    }

    public void PlayHitAnimation()
    {
        animator.SetTrigger(GlobalVar.IS_PLAYER_HIT_ANIMATION);
    }

    public void PlayDeathAnimation()
    {
        animator.SetBool(GlobalVar.IS_DEAD_ANIMATION, true);
    }

    public void Die()
    {
        Destroy(this.gameObject);
    }
}
