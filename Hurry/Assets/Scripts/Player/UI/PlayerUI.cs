﻿using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : Singleton<PlayerUI>
{
    [Header("Player Statistics UI")]
    [SerializeField] private ProgressBar progressHealthBar;
    [SerializeField] private ProgressBar progressShieldBar;

    [Header("Player Buff UI")]
    [SerializeField] private GameObject skullUIBuff;
    [SerializeField] private GameObject healthUIBuff;
    [SerializeField] private GameObject bubbleUIBuff;

    private Player player;
    private Camera mainCamera;

    #region Getters
    public GameObject SkullUIBuff { get => skullUIBuff; }
    public GameObject HealthUIBuff { get => healthUIBuff; }
    public GameObject BubbleUIBuff { get => bubbleUIBuff; }
    #endregion

    private void Start()
    {
        CheckDuplicates();
        player = FindObjectOfType<Player>();
        mainCamera = FindObjectOfType<Camera>();
        GetComponent<Canvas>().worldCamera = mainCamera;
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        progressHealthBar.currentPercent = player.GetCurrentHealthPercentage();
        progressShieldBar.currentPercent = player.GetCurrentShieldPercentage();
    }

    private void OnLevelWasLoaded(int level)
    {
        DestroyIfMenuSceneIsActive();
        skullUIBuff.SetActive(false);
        bubbleUIBuff.SetActive(false);
        healthUIBuff.SetActive(false);
        player = FindObjectOfType<Player>();
        mainCamera = FindObjectOfType<Camera>();
        GetComponent<Canvas>().worldCamera = mainCamera;
    }

    #region Utils
    private void DestroyIfMenuSceneIsActive()
    {
        if (SceneTransitionManager.Instance.GetCurrentSceneName() == GlobalVar.MAIN_MENU_SCENE_NAME)
        {
            Destroy(this.gameObject);
        }
    }

    private void CheckDuplicates()
    {
        if (FindObjectsOfType<PlayerUI>().Length > 1)
        {
            Destroy(this.gameObject);
        }
    }
    #endregion
}
