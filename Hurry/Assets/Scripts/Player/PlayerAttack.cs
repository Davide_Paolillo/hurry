﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private Animator animator;
    private PlayerController playerController;
    private Kick playerKick;
    private Punch playerPunch;

    private float prevMovementSpeed;
    private float prevJumpForce;
    private float animationPunchDuration;
    private float animationKickDuration;

    private void Start()
    {
        CachedParameters();
        ParametersInitialization();
        GetKickAndPunchAnimationTime();
    }

    #region InitializationFunctions

    private void ParametersInitialization()
    {
        prevMovementSpeed = playerController.MovementSpeed;
        prevJumpForce = playerController.JumpForce;
    }

    #region CachedParameters
    private void GetKickAndPunchAnimationTime()
    {
        var animatorController = animator.runtimeAnimatorController;

        foreach (var clip in animatorController.animationClips)
        {
            if (clip.name.Equals(GlobalVar.PLAYER_KICK_ANIMATION))
            {
                animationKickDuration = clip.length;
            }
            else if (clip.name.Equals(GlobalVar.PLAYER_PUNCH_ANIMATION))
            {
                animationPunchDuration = clip.length;
            }
        }
    }

    private void CachedParameters()
    {
        playerKick = FindObjectOfType<Kick>();
        playerPunch = FindObjectOfType<Punch>();
        animator = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
    }
    #endregion

    #endregion

    private void Update()
    {
        KickOnLeftMouseButtonDown();
        PunchOnRighMouseButtonDown();

        CancelPunchAnimation();
        CancelKickAnimation();

        CheckKickAndPunchDontOverlay();
    }

    #region PlayAnimations
    private void CheckKickAndPunchDontOverlay()
    {
        if(GetKickAnimation() && GetPunchAnimation())
        {
            EndPunch();
            EndKick();
            ResetMovementSpeed();
        }
    }

    private void CancelKickAnimation()
    {
        if (GetKickAnimation() && !playerKick.CanAttack)
        {
            EndKick();
            ResetMovementSpeed();
        }
    }

    private void CancelPunchAnimation()
    {
        if (GetPunchAnimation() && !playerPunch.CanAttack)
        {
            EndPunch();
            ResetMovementSpeed();
        }
    }

    private void KickOnLeftMouseButtonDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetBool(GlobalVar.PLAYER_KICK, true);
            animator.SetBool(GlobalVar.PLAYER_CROUCH_KICK_ANIMATION, true);
            StartCoroutine(EndKickAfterAnimationTimer());
            playerKick.PlayerKick();
        }
    }

    private void PunchOnRighMouseButtonDown()
    {
        if (Input.GetMouseButtonDown(1))
        {
            animator.SetBool(GlobalVar.PLAYER_PUNCH, true);
            StartCoroutine(EndPunchAfterAnimationTimer());
            playerPunch.PlayerPunch();
        }
    }

    private IEnumerator EndPunchAfterAnimationTimer()
    {
        yield return new WaitForSeconds(animationPunchDuration);
        EndPunch();
    }

    private IEnumerator EndKickAfterAnimationTimer()
    {
        yield return new WaitForSeconds(animationKickDuration);
        EndKick();
    }
    #endregion

    #region PlayDuringAnimationFrames
    public void ResetMovementSpeed()
    {
        playerController.MovementSpeed = prevMovementSpeed;
        playerController.JumpForce = prevJumpForce;
    }

    public void StopMove()
    {
        playerController.MovementSpeed = 0;
        playerController.JumpForce = 0;
    }

    public void EndKick()
    {
        animator.SetBool(GlobalVar.PLAYER_KICK, false);
        animator.SetBool(GlobalVar.PLAYER_CROUCH_KICK_ANIMATION, false);
        ResetMovementSpeed();
    }

    public void EndPunch()
    {
        animator.SetBool(GlobalVar.PLAYER_PUNCH, false);
        ResetMovementSpeed();
    }

    public void ShotPunchBall()
    {
        playerPunch.ShotPunchBall();
    }
    #endregion

    #region AnimationsGetter
    public bool GetKickAnimation()
    {
        return animator.GetBool(GlobalVar.PLAYER_KICK);
    }

    public bool GetPunchAnimation()
    {
        return animator.GetBool(GlobalVar.PLAYER_PUNCH);
    }
    #endregion
}
