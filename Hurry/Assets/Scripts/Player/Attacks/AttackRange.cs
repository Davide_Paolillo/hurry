﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_BULLET_TAG))
        {
            Destroy(collision.gameObject);
        }
    }
}
