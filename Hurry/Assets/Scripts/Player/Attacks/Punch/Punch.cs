﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punch : Attack
{
    [Header("Bullet")]
    [SerializeField] private GameObject punchBall;

    private Transform bulletPool;
    private GameObject player;

    private void Awake()
    {
        bulletPool = GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_BULLET_POOL_TAG).transform;
        canAttack = true;
        player = GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG).gameObject;
    }

    protected override void DealDamage(Enemy enemy)
    {
        enemy.GetDamage(damage, typeOfAttack);
    }

    protected override bool CheckContact()
    {
        var direction = Mathf.Sign(player.transform.localScale.x);
        var hit = Physics2D.Raycast(this.transform.position, Vector2.right * direction, raycastDistance, enemyLayer);
        if (hit.collider != null && hit.collider.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            //TODO clear
            Debug.DrawRay(this.transform.position, Vector2.right * direction * raycastDistance, Color.green);
            DealDamage(hit.collider.gameObject.transform.parent.GetComponent<Enemy>());
            return true;
        }
        else
        {
            //TODO clear
            Debug.DrawRay(this.transform.position, Vector2.right * direction * raycastDistance, Color.red);
            return false;
        }
    }

    public void PlayerPunch()
    {
        StartCoroutine(CheckPlayerPunch());
    }

    private IEnumerator CheckPlayerPunch()
    {
        var canPunchHit = playerAttack.GetPunchAnimation();
        while (canPunchHit && canAttack)
        {
            canPunchHit = playerAttack.GetPunchAnimation();
            if (CheckContact())
            {
                canPunchHit = false;
            }
            yield return new WaitForSeconds(0);
        }
        yield return new WaitForSeconds(0);
    }

    public void ShotPunchBall()
    {
        if (canAttack)
        {
            GameObject myPunchBall = Instantiate(punchBall, this.transform.position, punchBall.transform.rotation) as GameObject;
            myPunchBall.GetComponent<Bullet>().SetBulletDamage *= GameManager.Instance.GetPlayer.AttackMultiplier;
            myPunchBall.transform.parent = bulletPool;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            canAttack = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            canAttack = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            canAttack = true;
        }
    }
}
