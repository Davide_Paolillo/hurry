﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punchball : Bullet
{
    private float direction;
    private float initialXPos;

    private void Start()
    {
        direction = Mathf.Sign(GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG).transform.localScale.x);
        if (direction < 0)
        {
            this.transform.localScale = new Vector3(this.transform.localScale.x, direction * this.transform.localScale.y, this.transform.localScale.z);
        }
    }

    protected override void ShotBullet()
    {
        this.gameObject.transform.Translate(Vector2.down * direction * Time.deltaTime * bulletSpeed);
    }

    private void Update()
    {
        ShotBullet();
    }
}
