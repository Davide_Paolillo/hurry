﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kick : Attack
{
    private GameObject player;

    private void Awake()
    {
        canAttack = true;
        player = GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG);
    }

    protected override void DealDamage(Enemy enemy)
    {
        enemy.GetDamage(damage, typeOfAttack);
    }

    protected override bool CheckContact()
    {
        var direction = Mathf.Sign(player.transform.localScale.x);
        var hit = Physics2D.Raycast(this.transform.position, Vector2.right * direction, raycastDistance, enemyLayer);
        if(hit.collider != null && hit.collider.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            Debug.DrawRay(this.transform.position, Vector2.right * direction * raycastDistance, Color.green);
            DealDamage(hit.collider.gameObject.transform.parent.GetComponent<Enemy>());
            return true;
        }
        else
        {
            Debug.DrawRay(this.transform.position, Vector2.right * direction * raycastDistance, Color.red);
            return false;
        }
    }

    public void PlayerKick()
    {
        StartCoroutine(CheckKickContact());
    }

    private IEnumerator CheckKickContact()
    {
        var canKickHit = playerAttack.GetKickAnimation();
        while (canKickHit && canAttack)
        {
            canKickHit = playerAttack.GetKickAnimation();
            if (CheckContact())
            {
                canKickHit = false;
            }
            yield return new WaitForSeconds(0);
        }
        yield return new WaitForSeconds(0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            canAttack = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            canAttack = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            canAttack = true;
        }
    }
}
