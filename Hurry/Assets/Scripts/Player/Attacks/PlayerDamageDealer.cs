﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamageDealer : DamageDealer
{
    public override void DealDamage(GameObject enemy)
    {
        enemy.GetComponent<Enemy>().GetDamage(damage, typeOfBullet);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            Destroy(this.gameObject);
            DealDamage(collision.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_BULLET_TAG))
        {
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            Destroy(this.gameObject);
        }
    }
}
