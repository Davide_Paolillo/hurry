﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ShieldSource : BeneficalSources
{
    [Header("VFX")]
    [Range(0.01f, 0.1f)] [SerializeField] private float lightUpAndDownScale = 0.02f;
    [Range(0.01f, 0.1f)] [SerializeField] private float lightUpAndDownTimer = 0.05f;

    protected override void RegenStat()
    {
        StartCoroutine(CastRegenerationStat());
    }

    protected override IEnumerator CastRegenerationStat()
    {
        StartCoroutine(RegenerationPops());
        yield return new WaitForSeconds(TimeToLive);
        StopCoroutine(RegenerationPops());
        Destroy(this.gameObject);
    }

    protected override IEnumerator RegenerationPops()
    {
        var isRunning = false;
        while (true)
        {
            if (CanPop)
            {
                GameManager.Instance.GetPlayer.RegeneratePlayerShield(RegenerationPower);
            }

            if (!isRunning)
            {
                StartCoroutine(LightSourceUp());
                isRunning = true;
            }
            yield return new WaitForSeconds(CooldownBetweenRegenPops);
        }
    }

    private IEnumerator LightSourceUp()
    {
        do
        {
            while (GetComponentInChildren<Light2D>().intensity <= 1.0f)
            {
                GetComponentInChildren<Light2D>().intensity += lightUpAndDownScale;
                yield return new WaitForSeconds(lightUpAndDownTimer);
            }

            while (GetComponentInChildren<Light2D>().intensity > 0.5f)
            {
                GetComponentInChildren<Light2D>().intensity -= lightUpAndDownScale;
                yield return new WaitForSeconds(lightUpAndDownTimer);
            }
        }
        while (true);
    }
}
