﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform gun;

    private GameObject enemyBulletPool;

    private void Start()
    {
        enemyBulletPool = GameObject.FindGameObjectWithTag(GlobalVar.ENEMY_BULLET_POOL_TAG);
    }

    public void Fire()
    {
        GameObject shot = Instantiate(bullet, gun.transform.position, Quaternion.identity) as GameObject;
        shot.GetComponent<Bullet>().SetBulletDamage *= GetComponent<Enemy>().AttackMultiplier;
        if (shot.GetComponent<Fireball>())
        {
            shot.GetComponent<Fireball>().Direction = Mathf.Sign(this.transform.localScale.x);
        }
        shot.transform.parent = enemyBulletPool.transform;
    }
}
