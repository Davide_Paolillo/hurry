﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBuffs : MonoBehaviour
{
    private List<Buff> buffList;

    private void Start()
    {
        buffList = new List<Buff>();
    }

    public void AddEnemyBuff(Buff buff)
    {
        buffList.Add(buff);
        StartCoroutine(DecayBuff(buff));
    }

    private IEnumerator DecayBuff(Buff buff)
    {
        yield return new WaitForSeconds(buffList[buffList.IndexOf(buff)].BuffDuration());
        buffList[buffList.IndexOf(buff)].EndBuff(this.gameObject);
    }
}
