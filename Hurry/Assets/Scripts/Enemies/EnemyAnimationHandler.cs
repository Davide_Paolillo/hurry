﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationHandler : MonoBehaviour
{
    private Transform lookAtPlayer;
    private Animator animator;
    private bool canShoot;

    public bool CanShoot { get => canShoot; }

    private void Start()
    {
        lookAtPlayer = GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG).transform;
        animator = GetComponent<Animator>();
        canShoot = false;
    }

    private void Update()
    {
        if (GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG) == null) { return;  }
        FlipWizardBasedOnPlayerPosition();
    }

    private void FlipWizardBasedOnPlayerPosition()
    {
        if (lookAtPlayer.position.x > this.transform.position.x)
        {
            this.transform.localScale = new Vector3(-1 * Mathf.Abs(this.transform.localScale.x), 1 * Mathf.Abs(this.transform.localScale.x), 1 * Mathf.Abs(this.transform.localScale.x));
        }
        else
        {
            this.transform.localScale = new Vector3(1 * Mathf.Abs(this.transform.localScale.x), 1 * Mathf.Abs(this.transform.localScale.x), 1 * Mathf.Abs(this.transform.localScale.x));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            StartShoot();
        }
    }

    private void StartShoot()
    {
        canShoot = true;
        animator.SetBool("CanAttack", canShoot);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            StartShoot();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            StopShoot();
        }
    }

    private void StopShoot()
    {
        canShoot = false;
        animator.SetBool("CanAttack", canShoot);
    }

    public float PlayDeathAnimationTimer()
    {
        animator.SetTrigger(GlobalVar.ENEMY_DEATH_TAG);
        return animator.GetCurrentAnimatorClipInfo(0).Length;
    }
}
