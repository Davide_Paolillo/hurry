﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenWizard : Enemy
{
    public override Enemy Clone()
    {
        return (Enemy)this.MemberwiseClone();
    }

    public override void GetDamage(float damage, TypeOfAttack typeOfAttack)
    {
        if (BuffShieldHits > 0)
        {
            BuffShieldHits -= 1;
        }
        else
        {
            switch (typeOfAttack)
            {
                case TypeOfAttack.Physical:
                    var percentageOfMitigatedPhysicalDamage = enemyStatistics.Armor * damage / 100;
                    this.health -= (damage - percentageOfMitigatedPhysicalDamage);
                    break;
                case TypeOfAttack.Magical:
                    var percentageOfMitigatedMagicalDamage = enemyStatistics.MagicResist * damage / 100;
                    this.health -= (damage - percentageOfMitigatedMagicalDamage);
                    break;
                default:
                    break;
            }
        }
    }
}
