﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : Bullet
{
    [HideInInspector]
    [SerializeField] public bool shouldFollowPlayer = false;

    [HideInInspector]
    [SerializeField] public GameObject playerToFollow;

    private float direction;
    private bool shouldFollow;

    public float Direction { get => direction; set => direction = value; }

    private void Start()
    {
        shouldFollow = shouldFollowPlayer;
        if (shouldFollow)
        {
            playerToFollow = GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG);
        }
    }

    protected override void ShotBullet()
    {
        if (shouldFollow && GameObject.FindGameObjectWithTag(GlobalVar.PLAYER_TAG) != null)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, playerToFollow.transform.position, bulletSpeed * Time.deltaTime);
        }
        else
        {
            this.gameObject.transform.Translate(Vector2.left * direction * Time.deltaTime * bulletSpeed);
        }
    }

    private void Update()
    {
        ShotBullet();
    }
}
