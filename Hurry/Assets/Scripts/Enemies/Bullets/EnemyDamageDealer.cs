﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamageDealer : DamageDealer
{
    public override void DealDamage(GameObject enemy)
    {
        enemy.GetComponent<Player>().GetDamage(damage, typeOfBullet);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            DealDamage(collision.gameObject);
            /* Because destroy happens at the end of the excution time, and a sphere has multiple contact points, so we disable the
            collider to avoid multiple collisions detection. */
            GetComponent<CircleCollider2D>().enabled = false;
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_KICK) || collision.gameObject.tag.Equals(GlobalVar.PLAYER_PUNCH))
        {
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.GROUND_TAG))
        {
            Destroy(this.gameObject);
        }
    }
}
