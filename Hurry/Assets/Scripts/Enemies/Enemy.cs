﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [Header("Basic Stats")]
    [Range(1.0f, 500.0f)][SerializeField] protected float health;
    [Range(0.0f, 70.0f)][SerializeField] private float armor;
    [Range(0.0f, 70.0f)] [SerializeField] private float magicResist;

    [Header("Buffable Stats")]
    [Range(0.0f, 5.0f)][SerializeField] private float healthMultiplier = 1.0f;
    [Range(1.0f, 2.0f)][SerializeField] private float attackMultiplier = 1.0f;
    [SerializeField] private int buffShieldHits;

    [Header("Buff Items")]
    [SerializeField] private GameObject shield;

    protected Resistances enemyStatistics;

    private float initalHealth;
    private float maxHealth;

    public float Health { get => health; }
    public float AttackMultiplier { get => attackMultiplier; set => attackMultiplier = value; }
    public int BuffShieldHits { get => buffShieldHits; set => buffShieldHits = value; }
    public float HealthMultiplier { get => healthMultiplier; set => healthMultiplier = value; }

    private void Start()
    {
        CheckFields();
        enemyStatistics = new Resistances(armor, magicResist, healthMultiplier);
        InitHealthStat();
    }

    private void CheckFields()
    {
        if(attackMultiplier < 0.5f)
        {
            attackMultiplier = 0.5f;
        }

        if (healthMultiplier < 1.0f)
        {
            healthMultiplier = 1.0f;
        }
    }

    private void Update()
    {
        CheckIfEnemyIsAlive();
        ToggleShield();
        HealthPercentageCheck();
    }

    private void ToggleShield()
    {
        if (BuffShieldHits > 0)
        {
            shield.SetActive(true);
        }
        else
        {
            buffShieldHits = 0;
            shield.SetActive(false);
        }
    }

    #region HealthOperations
    private void CheckIfEnemyIsAlive()
    {
        if(health <= 0)
        {
            KillEnemy();
        }
    }

    private void InitHealthStat()
    {
        initalHealth = health;
        health = initalHealth * enemyStatistics.HealthMultiplier;
        maxHealth = initalHealth * enemyStatistics.HealthMultiplier;
    }

    private void HealthPercentageCheck()
    {
        maxHealth = initalHealth * enemyStatistics.HealthMultiplier;
        enemyStatistics = new Resistances(armor, magicResist, healthMultiplier);
        var newMaxHealth = initalHealth * enemyStatistics.HealthMultiplier;
        if (newMaxHealth != maxHealth)
        {
            var percentageOfHealth = health * 100 / maxHealth;
            health = newMaxHealth * percentageOfHealth / 100;
        }
    }
    #endregion

    public abstract Enemy Clone();
    public abstract void GetDamage(float damage, TypeOfAttack typeOfAttack);

    private void KillEnemy()
    {
        Destroy(this.gameObject, GetComponent<EnemyAnimationHandler>().PlayDeathAnimationTimer());
    }
}
