﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private Animator menuAnimator;
    [SerializeField] private Animator hamburgerMenuAnimator;
    [SerializeField] private Animator scoreMenuAnimator;
    [SerializeField] private Animator resetDataButton;

    private float timeToWaitForResetTrigger;

    private void Start()
    {
        timeToWaitForResetTrigger = 0.2f;
    }

    public void HandleMenuClicks()
    {
        if (hamburgerMenuAnimator.GetCurrentAnimatorStateInfo(0).IsName(GlobalVar.OPEN_HAMBURGER_MENU_ANIMATION))
        {
            CloseOptionsMenu();
        }
        else if (hamburgerMenuAnimator.GetCurrentAnimatorStateInfo(0).IsName(GlobalVar.CLOSE_HAMBURGER_MENU_ANIMATION))
        {
            OpenOptionsMenu();
        }
        else
        {
            OpenOptionsMenu();
        }
    }

    public void HandleScoreMenuClicks()
    {
        if (scoreMenuAnimator.GetCurrentAnimatorStateInfo(0).IsName(GlobalVar.OPEN_SCORE_TEXT_ANIMATION))
        {
            CloseScoreTextFields();
        }
        else if (scoreMenuAnimator.GetCurrentAnimatorStateInfo(0).IsName(GlobalVar.CLOSE_SCORE_TEXT_ANIMATION))
        {
            OpenScoreTextFields();
        }
        else
        {
            OpenScoreTextFields();
        }
    }

    public void HandleResetDataClicks()
    {
        ResetData();
    }

    private void ResetData()
    {
        PlayerPrefs.DeleteAll();
        StartCoroutine(WaitSecondsBeforeResetButton());
    }

    private IEnumerator WaitSecondsBeforeResetButton()
    {
        yield return new WaitForSeconds(0.5f);
        resetDataButton.Play(GlobalVar.CLOSE_HAMBURGER_MENU_ANIMATION);
        SceneTransitionManager.Instance.ReloadCurrentScene(new List<GameObject>());
    }

    private void OpenScoreTextFields()
    {
        scoreMenuAnimator.Play(GlobalVar.OPEN_SCORE_TEXT_ANIMATION);
    }

    private void CloseScoreTextFields()
    {
        scoreMenuAnimator.Play(GlobalVar.CLOSE_SCORE_TEXT_ANIMATION);
    }

    private void OpenOptionsMenu()
    {
        menuAnimator.SetTrigger(GlobalVar.OPEN_OPTIONS_MENU_ANIMATION);
        StartCoroutine(ResetTrigger(GlobalVar.OPEN_OPTIONS_MENU_ANIMATION));
    }

    private void CloseOptionsMenu()
    {
        menuAnimator.SetTrigger(GlobalVar.CLOSE_OPTIONS_MENU_ANIMATION);
        StartCoroutine(ResetTrigger(GlobalVar.CLOSE_OPTIONS_MENU_ANIMATION));
    }

    public void PlayTheGame()
    {
        SceneTransitionManager.Instance.GoToScene(GlobalVar.LEVEL_ONE_SCENE, new List<GameObject>());
    }

    private IEnumerator ResetTrigger(string param)
    {
        yield return new WaitForSeconds(timeToWaitForResetTrigger);
        menuAnimator.ResetTrigger(param);
    }
}
