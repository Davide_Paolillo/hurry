﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EndGameCanvas : MonoBehaviour
{
    protected Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public abstract void PlayAgain();
    public virtual void PlayCanvasAnimation() { }

    public void GoToMainMenu()
    {
        SceneTransitionManager.Instance.GoToScene(GlobalVar.MAIN_MENU_SCENE_NAME, new List<GameObject>());
    }
}
