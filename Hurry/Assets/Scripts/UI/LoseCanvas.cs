﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCanvas : EndGameCanvas
{
    public override void PlayAgain()
    {
        GameManager.Instance.IsPlayerAlive = true;
        SceneTransitionManager.Instance.ReloadCurrentScene(new List<GameObject>());
    }
}
