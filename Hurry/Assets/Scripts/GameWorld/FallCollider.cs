﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            //Destroy(collision.gameObject);
            GameManager.Instance.IsPlayerAlive = false;
        }
    }
}
