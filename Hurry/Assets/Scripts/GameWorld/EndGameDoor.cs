﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameDoor : MonoBehaviour
{
    private bool isRunning;

    private void Start()
    {
        isRunning = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG) && !isRunning)
        {
            StartCoroutine(LoadScene());
        }
    }

    private IEnumerator LoadScene()
    {
        isRunning = true;
        SceneTransitionManager.Instance.GoToScene(SceneTransitionManager.Instance.GetMainMenuScene(), new List<GameObject>());
        yield return new WaitForSeconds(1.0f);
        isRunning = false;
    }
}
