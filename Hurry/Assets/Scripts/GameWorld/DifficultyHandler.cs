﻿using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyHandler : Singleton<DifficultyHandler>
{
    private HorizontalSelector horizontalSelector;
    private Difficulty currentDifficulty;

    private int difficultyMultiplier;

    public Difficulty CurrentDifficulty { get => currentDifficulty; }

    private void Start()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType<DifficultyHandler>().Length > 1)
        {
            Destroy(this.gameObject);
        }

        InitializeDifficulty();
    }

    #region Initializations
    private void InitializeDifficulty()
    {
        difficultyMultiplier = PlayerPrefs.HasKey(GlobalVar.PLAYER_PREF_DIFFICULTY_NAME) ? 
            PlayerPrefs.GetInt(GlobalVar.PLAYER_PREF_DIFFICULTY_NAME) : -1;
        SetNewDifficulty();
    }
    #endregion

    private void Update()
    {
        CheckDifficultyModifier();
    }

    #region CalculationMethods
    private void CheckDifficultyModifier()
    {
        if (SceneTransitionManager.Instance.GetCurrentSceneName().Equals(GlobalVar.MAIN_MENU_SCENE_NAME))
        {
            if (FindObjectOfType<HorizontalSelector>())
            {
                horizontalSelector = FindObjectOfType<HorizontalSelector>();
                if (horizontalSelector.label.text.Contains(GlobalVar.EASY_DIFFICULTY_LABEL))
                {
                    difficultyMultiplier = (int) Difficulty.Easy;
                }
                else if (horizontalSelector.label.text.Contains(GlobalVar.NORMAL_DIFFICULTY_LABEL))
                {
                    difficultyMultiplier = (int) Difficulty.Normal;
                }
                else
                {
                    difficultyMultiplier = (int) Difficulty.Hard;
                }
                SetNewDifficulty();
            }
        }
    }

    private void SetNewDifficulty()
    {
        switch (difficultyMultiplier)
        {
            case 0:
                currentDifficulty = Difficulty.Easy;
                break;
            case 1:
                currentDifficulty = Difficulty.Normal;
                break;
            case 2:
                currentDifficulty = Difficulty.Hard;
                break;
            default:
                currentDifficulty = Difficulty.Easy;
                break;
        }
    }

    public void SetCurrentLelvelDifficulty(Player player, Enemy[] enemies)
    {
        switch (currentDifficulty)
        {
            case Difficulty.Easy:
                player.RegeneratePlayerShield(player.MaxShield);
                foreach(var enemy in enemies)
                {
                    enemy.AttackMultiplier *= 0.5f;
                }
                break;
            case Difficulty.Normal:
                break;
            case Difficulty.Hard: // TODO Fix this
                player.AttackMultiplier /= 2.0f;
                foreach (var enemy in enemies)
                {
                    enemy.HealthMultiplier += 0.5f;
                }
                break;
            default:
                throw new ArgumentException("Difficulty is not set");
        }
    }
    #endregion
}
