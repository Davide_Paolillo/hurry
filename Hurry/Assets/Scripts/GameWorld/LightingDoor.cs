﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightingDoor : MonoBehaviour
{
    private Light2D doorLight;

    private bool isRunning;
    private float maxIllumination;

    void Start()
    {
        maxIllumination = 1.0f;
        doorLight = GetComponent<Light2D>();
        isRunning = false;
    }

    void Update()
    {
        if (!isRunning)
        {
            if (doorLight.intensity >= maxIllumination)
            {
                StartCoroutine(LightUp());
            }
            else
            {
                StartCoroutine(LightDown());
            }
        }
    }

    #region Coroutines
    private IEnumerator LightUp()
    {
        isRunning = true;
        while (doorLight.intensity > 0)
        {
            doorLight.intensity -= maxIllumination * Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
        isRunning = false;
    }

    private IEnumerator LightDown()
    {
        isRunning = true;
        while (doorLight.intensity <= maxIllumination)
        {
            doorLight.intensity += maxIllumination * Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
        isRunning = false;
    }
    #endregion
}
