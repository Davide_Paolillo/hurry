﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelDoor : MonoBehaviour
{
    [SerializeField] private float stallTime = 2.0f;
    [Range(0.0f, 1.0f)][SerializeField] private float timeScaleWhenLevelEnd = 0.5f;

    private PlayerController playerController;

    private bool isRunning;

    private void Start()
    {
        isRunning = false;
        playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG) && !isRunning)
        {
            StartCoroutine(SceneTransitionManager.Instance.GetCurrentSceneBuildIndex() == SceneTransitionManager.Instance.GetSceneCountInBuild() - 1 ?
                LoadMainMenuScene() : LoadScene());
        }
    }

    private IEnumerator LoadScene()
    {
        isRunning = true;
        Instantiate(GameManager.Instance.WinCanvas);
        Time.timeScale = timeScaleWhenLevelEnd;
        yield return new WaitForSeconds(stallTime);
        Time.timeScale = 1.0f;
        SceneTransitionManager.Instance.GoToNextScene(SceneTransitionManager.Instance.GetNextSceneBuildIndex(), new List<GameObject>());
        isRunning = false;
    }

    private IEnumerator LoadMainMenuScene()
    {
        isRunning = true;
        Instantiate(GameManager.Instance.WinCanvas);
        ScoreManager.Instance.AddEndingGameScore();
        GameTimer.Instance.SaveGameSessionTime();
        Time.timeScale = timeScaleWhenLevelEnd;
        yield return new WaitForSeconds(stallTime);
        Time.timeScale = 1.0f;
        SceneTransitionManager.Instance.GoToNextScene(SceneTransitionManager.Instance.GetMainMenuSceneBuildIndex(), new List<GameObject>());
        isRunning = false;
    }
}
